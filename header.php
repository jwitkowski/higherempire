<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>HireEmpire</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="js/shadowbox-3.0.3/shadowbox.js"></script>
<link rel="stylesheet" type="text/css" href="js/shadowbox-3.0.3/shadowbox.css">
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,400italic,700italic' rel='stylesheet' type='text/css'>
<script type="text/javascript">
$(function() {
	var pull 		= $('#pull');
		menu 		= $('nav ul');
		menuHeight	= menu.height();

	$(pull).on('click', function(e) {
		e.preventDefault();
		menu.slideToggle();
	});
});

$(window).resize(function(){
	var w = $(window).width();
	if(w > 600 && menu.is(':hidden')) {
		menu.removeAttr('style');
	}
});	

Shadowbox.init();
</script>
</head>

<body>
	<div id="container">
		<header>
			<div class="unit two-of-five mobile-nav">
			    	<nav>
				       <ul>
				       		<a href="how-it-works.php"><li>How it works</li></a>
				       		<a href="#"><li>Our Blog</li></a>
				       		<a href="contact.php"><li>contact.php</li></a>
				       </ul>
					  <a href="#" id="pull"></a>  
					</nav>
			    </div>
			
			    <div class="unit two-of-four">
			        <a href="index.php"><img src="images/logo.gif" alt="higherEmpire"/></a>
			    </div>
			    <div class="unit two-of-five desk-nav">
			    	<nav>
				       <ul>
				       		<a href="how-it-works.php"><li>How it works</li></a>
				       		<a href="#"><li>Our Blog</li></a>
				       		<a href="contact.php"><li>Contact Us</li></a>
				       </ul>
					</nav>
			    </div>
			    <div class="unit two-of-two">
			    	<div class="mobile-login"><a href="#">Login</a></div><!--/mobile login-->
			    	<form>
			    		<label>
			    			Username<br/>
			    			<input type="text" name="username">
			    		</label>
			    		<label>
			    			Password<br/>
			    			<input type="text" name="username">
			    		</label>
			    		<input type="submit" value="Log in">
			    	</form>
			    </div>
			   
			
		</header>