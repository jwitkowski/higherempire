
	<?php include("header.php"); ?>	
		<div class="content">
			<a href="how-it-works.php"><h2 class="learn-holder-mobile">Learn how it works</h2></a>
			<h1>We connect</h1>
			<div class="unit one-of-two blue-top">
				<img src="images/employee.png" class="employee"alt="employee"/>
				<div class="three-of-four job-seeker">
					<h2>JOB SEEKERS</h2>
					<ul>
						<li>Go beyond the resume with a custom profile</li>
						<li>Jump-start your job search with a free dashboard</li>
						<li><strong>Contact <span>employers</span> directly - no middle-man</strong></li>
					</ul>
					<a href="signup-job-seekers.php"><button>Find your job</button></a>
				</div><!--three-of-four-->
			</div> <!--one-of-two-->
			<div class="middle-column">&amp;</div>
			<div class="unit one-of-two blue-top align-right">
				<img src="images/employer.png" class="employee"alt="employee"/>
				<div class="three-of-four employer">
					<h2>Employers</h2>
					<ul>
						<li>Broadcast postings in an extensive social network</li>
						<li>Find candidates who apply to employers like you</li>
						<li><strong>Contact <span>job seekers</span> directly - no middle-man!</strong></li>
					</ul>
					<a href="signup-employers.php"><button>Fill your position</button></a>
				</div><!--/three of 4-->
			</div> <!--/one-of-two-->
			<div class="learn-holder"><a href="how-it-works.php"><button class="learn">Learn how it works</button></a></div>
		</div><!--/content-->
		<div class="video">

		</div><!--/video-->
		<div class="content">
			<div class="span-grid">
			<h2 class="learn-holder-mobile">Get Started Now!</h2>
			<div class="blue-top">
				<button><img src="images/guy_head.png"/>Find your job</button>
				<button>Fill your position<img src="images/girl_head.png" class="align-right"/></button>
			</div>
			</div><!--/span-grid-->
			<footer>
				<div class="unit one-of-two blue-bottom">
					&copy;2013 HireEmpire. All rights reserved. <span><a href="contact.php">Contact Us</a></span>       
				</div>
				<div class="unit one-of-two blue-bottom">
					<ul>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="contact.php">Contact Us</a></li>
					</ul>  
					<ul class="sm-icons">
						<li><img src="images/icon_fb.jpg" alt="facebook"/></li>
						<li><img src="images/icon_twitter.jpg" alt="twitter"/></li>
						<li><img src="images/icon_googlePlus.jpg" alt="twitter"/></li>
						<li><img src="images/icon_linkedIn.jpg" alt="twitter"/></li>
					</ul>    
				</div>
			</footer>
		</div>

	</div><!--/container-->

</body>

</html>
	